#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
:author:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:copyright:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)
"""
from obspy import UTCDateTime
from obspy.clients.fdsn.client import Client
from tkinter import Tk, ttk


class listenearthgui:

    def __init__(self):
        self.root = Tk()
        self.root.title("Listen Earth !")
        # URL Frame
        self.fUrl = ttk.Frame(self.root)
        self.lUrl = ttk.Label(self.fUrl, text="FDSN webservice URL")
        self.eUrl = ttk.Entry(self.fUrl)
        self.eUrl.insert(0, 'RESIF')
        self.fUrl.pack()
        self.lUrl.pack()
        self.eUrl.pack()
        # Seed id Frame
        self.fSeedid = ttk.Frame(self.root)
        self.lSeedid = ttk.Label(self.fSeedid, text="Seed id")
        self.eSeedid = ttk.Entry(self.fSeedid)
        self.eSeedid.insert(0, 'G.CCD.20.BHZ')
        self.fSeedid.pack()
        self.lSeedid.pack()
        self.eSeedid.pack()
        # Periods
        self.fPeriod = ttk.Frame(self.root)
        self.lStart = ttk.Label(self.fPeriod, text="Start time")
        self.eStart = ttk.Entry(self.fPeriod)
        self.eStart.insert(0, "2022-07-01T00:00:00")
        self.lEnd = ttk.Label(self.fPeriod, text="End time")
        self.eEnd = ttk.Entry(self.fPeriod)
        self.eEnd.insert(0, "2022-07-02T00:00:00")
        self.fPeriod.pack()
        self.lStart.pack()
        self.eStart.pack()
        self.lEnd.pack()
        self.eEnd.pack()
        # Get it!
        self.fButton = ttk.Frame(self.root)
        self.bButton = ttk.Button(self.fButton, text='Get Sound file!',
                                  command=self.getWaveform_n_write)
        self.fButton.pack()
        self.bButton.pack()
        self.root.mainloop()

    def getWaveform_n_write(self):
        url = self.eUrl.get()
        seedid = self.eSeedid.get()
        starttime = UTCDateTime(self.eStart.get())
        endtime = UTCDateTime(self.eEnd.get())
        wsclient = Client(base_url=url)
        codes = seedid.split('.')
        st = wsclient.get_waveforms(codes[0], codes[1], codes[2], codes[3],
                                    starttime, endtime)
        if len(st):
            st.plot(equal_scale=False)
            for tr in st:
                tr.data = tr.data*2**31/tr.data.max()
                filename = "%s-%d-%02d-%02dT%02d%02d%02d" % \
                    (tr.id.replace('.', '-'),
                     tr.stats.starttime.year,
                     tr.stats.starttime.month,
                     tr.stats.starttime.day,
                     tr.stats.starttime.hour,
                     tr.stats.starttime.minute,
                     tr.stats.starttime.second)
                tr.write(filename, framerate=44100, format='WAV')


gui = listenearthgui()
