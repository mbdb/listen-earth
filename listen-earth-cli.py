#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
:author:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:copyright:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)

Usage:
    ./listen-earth-cli <start> <end> <stream> [--url=<url>]

Options:
    -h --help           Show this screen.
    --version           Show version.
    <start>             Start time in obspy.core.UTCDateTime format.
    <end>               End time in obspy.core.UTCDateTime format.
    <stream>            Stream to analyse in seed code separated by dots. \
Multiple streams are allowed, separated by comma \
(Ex: XX.GPIL.00.HH?,FR.STR.00.HH?).
    --url=<url>         Set base url of fdsnws server, fdsn mode only. \
[default: RESIF]
"""

from docopt import docopt
from obspy.clients.fdsn import Client
from obspy.core import UTCDateTime, Stream


if __name__ == '__main__':
    args = docopt(__doc__, version='SeismoSound 1.0')
    # Uncomment for debug
    # print(args)

    wsclient = Client(base_url=args['--url'])
    t1 = UTCDateTime(args['<start>'])
    t2 = UTCDateTime(args['<end>'])
    streams = args['<stream>'].split(',')
    st = Stream()
    for stream in streams:
        code = stream.split('.')
        st += wsclient.get_waveforms(code[0], code[1], code[2], code[3],
                                     t1, t2)
    if len(st):
        print(st)
        for tr in st:
            tr.data = tr.data*2**31/tr.data.max()
            filename = "%s-%d-%02d-%02dT%02d%02d%02d.wav" % \
                (tr.id.replace('.', '-'),
                 tr.stats.starttime.year,
                 tr.stats.starttime.month,
                 tr.stats.starttime.day,
                 tr.stats.starttime.hour,
                 tr.stats.starttime.minute,
                 tr.stats.starttime.second)
            tr.write(filename, framerate=44100, format='WAV')
