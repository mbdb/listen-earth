# Listen Earth
Ce projet a pour objectif de fournir un outil simple permettant de télécharger des données sismologiques et de les enregistrer en fichier son (format WAV).  
Ce travail a été effectué en partie en collaboration avec [Anaïs Dunn](https://anaisdunn.com).  
Les données sont issues de stations de mesures et distribuées par les centres de données sismologiques mondiaux mettant à disposition leurs données par un webservice tel que défini par la [FDSN](https://fdsn.org) (International Federation of Digital Seismograph Networks). Leurs conditions d'utilisation sont à examiner au cas par cas et il appartient à l'utilisateur de les appliquer.

## 1ère étape: connaître l'identifiant du canal recherché
L'utilisateur doit connaître le code du réseau, de la station et du canal qui l'intéresse. L'identifiant est décrit par la syntaxe suivante:  
NN.SSSSS.LL.CCC avec:  
NN le code réseau (2 caractères)  
SSSS le code station (3 à 5 caractères)  
LL le code localisation (2 caractères)  
CCC le code canal  (3 caractères)  
L'identifiant se détermine en parcourant la description du réseau sur la page web de l'opérateur, tenu de fournir des informations exhaustives sur chacun de leurs intruments. Ci-dessous une description plus précise de la façon dont les codes se déterminent.
### Code réseau
Les réseaux sismologiques publics sont décrits sur le site de la fdsn [FDSN: Network codes](https://fdsn.org/networks/). La plupart des réseaux permanents fournissent des données publiques sans restriction.  
Examples de réseaux intéressants:  
- FR: [RLBP](https://rlbp.resif.fr/) (Réseau français métropolitain)  
- G: [Geoscope](https://geoscope.ipgp.fr) (Réseau français à couverture mondiale)  
- II: [IDA](https://ida.ucsd.edu/) (Réseau américain à couverture mondiale)  
- IU: [ASL](https://www.usgs.gov/programs/earthquake-hazards/gsn-global-seismographic-network) (Réseau américain à couverture mondiale)  
### Code station
Le code station est composé de 3 à 5 caractères. A déterminer sur la page de description du réseau.  
### Code localisation
Le code de localisation est composé de 0 à 2 caractères. Il permet de discriminer quel capteur utiliser lorsque plusieurs capteurs sont installés sur une même station. Une convention bien admise dans la plupart des réseaux consiste à renseigner le code 00 pour le meilleur capteur de la station. A déterminer sur la page de description de la station. 00 peut être utilisé par défaut.  
### Code canal
Le code canal est composé de 3 caractères, chacun ayant une signification:  
1. Taux d'échantillonnage (ie. le nombre d'échantillons par seconde).
  * H: 100 Hz (pour écouter les séismes modérés sur les réseaux nationaux)
  * B: 20 Hz (pour écouter les séismes majeurs sur les réseaux mondiaux)
  * L: 1 Hz (pour écouter le bruit terrestre)
2. Type de capteur. Il exite 3 types principaux:  
  - H (High gain) pour les capteurs actifs.  
  - L (Low gain) pour les capteurs passifs.  
  - N pour les accéléromètres.  
3. Composante:  
  - Z: vertical  
  - N: nord  
  - E: est  
  - 1: voie horizontale d'orientation arbitraire  
  - 2: voie horizontale d'orientation arbitraire perpendiculaire à la précédente  
Un taux d'échantillonage plus grand permet d'écouter des évènements proches (bruits de l'activité humaine, séismes proches), un taux d'échantillonnage plus faible permet d'écouter des séismes plus lointains (donc plus forts) et les bruits de l'activité terrestre (pic microsismique (ie. résonance de l'océan sur le manteau), marées terrestres, ...). B peut être utilisé par défaut.  
Dans la plupart des réseaux permanents, à l'exception des réseaux spécialisés tels que les réseaux accélérométriques, les capteurs sont actifs. H peut être utilisé par défaut. 
Quasiment tous les capteurs aujourd'hui comportent 3 composantes. Il sont notés Z, N et E si le capteur a été installé orienté , ou Z, 1, 2 si le capteur a été installé avec une orientation arbitraire (au fond d'un forage par exemple). Certains réseaux nomment leurs voies horizontales 1 et 2 systématiquement pour ne plus faire de distinction. La voie Z est quasiment systématiquement utilisée et peut être utilisée par défaut.  
Il est possible de renseigner plusieurs voies en même temps grâce aux caractères joker: '?' pouvant remplacer un caractère, '*' une chaine de caractère. 
### Examples
- G.CCD.00.BHZ: Voie verticale à 20 Hz du capteur principal de Concordia (Antarctique) du réseau Geoscope.
- G.CCD.00.BH?: Toutes les voies à 20 Hz de Concordia.
- G.*.00.BHZ: Toutes les voies verticales à 20 Hz du réseau Geoscope (gros afflux de données à prévoir).
- FR.BOUC.00.HH1: Voie horizontale n°1 à 100Hz du capteur principal en forage de Bouclans (Franche-Comté, France) du réseau RLBP.
- IU.KIP.00.LHZ: Voie verticale du capteur principal de Hawaïi du réseau ASL à 1 Hz.

## 2ème étape: connaître l'url du centre de données en charge de la distribution
La page dédiée au réseau doit donner un lien vers le centre de données en charge de la distribution. La plupart des url des webservices des centres de données sont pointés par des mots clés faisant office d'alias:  
- *IPGP* -> *http://ws.ipgp.fr* pour le réseau *G*.  
- *RESIF* -> *http://ws.resif.fr* pour le réseau *FR*.  
- *IRIS* -> *http://service.iris.edu* pour les réseaux *IU* et *II*.  
Les programmes étant basés sur la bibliothèque obspy, une liste exhaustive est disponible [ici](https://docs.obspy.org/packages/obspy.clients.fdsn.html). Si le centre de données voulu n'est pas dans la liste, il convient de remplir l'url complète (http://url:port).

## 3ème étape: définir la période de signal à écouter. 
Les champs starttime et endtime sont à remplir selon la norme iso8601 (ie. au format AAAA-MM-JJThh:mm:ss). Le format peut ne pas être complet après le jour.  
Examples: 
- 2022-11-01 (1er novembre 2022 à minuit)  
- 2022-11-01T00:10 (1er novembre 2022 à minuit dix)  
- 2022-11-01T03:17:54 (1er novembre 2022 à 3h17m54s)  
Pour définir une période adéquate, il peut être utile de consulter les catalogues de séismes remarquables. Quelques instituts fournissent ce genre de catalogue:
- [Renass](https://renass.unistra.fr)
- [Neic](https://www.usgs.gov/programs/earthquake-hazards/national-earthquake-information-center-neic)

## 4ème étape: récupérer les données et effectuer la conversion
Le programme récupère les données voulues au centre de données spécifiés, convertit le taux d'échantillonnage en un taux audible (44100 Hz), normalise l'amplitude sur 31 bits et écrit le fichier son. Ce dernier est nommé en fonction de son identifiant et de sa date de début. Si la trace récupérée comporte plusieurs portions ou plusieurs voies, autant de fichiers seront écrits.

## Examples remarquables:
- Seisme de Tohoku (Mw 9.1 le 11 mars 2011 au Japon) à la station de Concordia (Antarctique): `./listen-earth-cli.py 2011-03-11 2011-03-12 G.CCD.00.BHZ --url=IPGP`  
- Vélage du glacier de l'Astrolabe (le 05 novembre 2021) à la station de Dumont d'Urville (Antarctique): `./listen-earth-cli.py ./listen-earth-cli.py 2021-11-05T06 2021-11-05T18 G.DRV.00.BHZ --url=IPGP`  
- Marsquake (Tremblement de Mars) de M5 le 4 mai 2022 enregistré par la mission InSight et l'instrument SEIS: `./listen-earth-cli.py 2022-05-04T23 2022-05-05T01 XB.ELYSE.00.HHU --url=IPGP`  

